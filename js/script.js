'use strict'
var Matrix = function (m, n, x) {
    this.id = 1;
    this.x = x;
    if (m > 0 && n > 0 && m < 100 && n < 100) {
        this.table = this.createTable(m, n);
    } else {
        this.table = [];
        alert('too much cols/rows');
    }

};
Matrix.prototype = {
    createTable: function (m, n) {
        var table = [];
        for (var i = 0; i < m; i++) {
            var row = [];
            for (var j = 0; j < n; j++) {
                var item = {
                    id: this.id++,
                    amount: Math.floor(Math.random() * 1000)
                };
                row.push(item);
            }
            table.push(row);
        }
        return table;
    },
    getRowSum: function (row) {
        if (row >= 0 && row < this.table[0].length) {
            var sum = 0;
            for (var col = 0; col < this.table.length; col++) {
                sum = sum + this.table[col][row].amount;
            }
            return sum;
        }
    },
    getColVal: function (col) {//замiнити на нормальний перевод
        if (col >= 0 && col < this.table.length) {
            var result = 0;
            for (var item = 0; item < this.table[col].length; item++) {
                result = result + this.table[col][item].amount;
            }
            result = result / this.table[col].length;
            result = result.toFixed(2);
            return result;
        }
    },
    render: function () {
        var table = document.getElementById('table');
        table.appendChild(this.renderPercent());
        var tbody = document.createElement('tbody');
        for (var row = 0; row < this.table[0].length; row++) {
            tbody.appendChild(this.renderMatrix(row));
        }
        table.appendChild(tbody);
    },
    renderPercent: function () {
        var th = document.createElement('thead');
        for (var col = 0; col < this.table.length; col++) {
            var td = document.createElement('td');
            var text = document.createTextNode(this.getColVal(col));
            td.appendChild(text);
            th.appendChild(td);
        }
        return th;
    },
    renderMatrix: function (row) {
        var tr = document.createElement('tr');
        for (var col = 0; col < this.table.length; col++) {
            var td = document.createElement('td');
            td.setAttribute('data-col', col);
            td.setAttribute('data-row', row);
            td.className = 'textVal';
            td.id = this.table[col][row].id;
            var text = document.createTextNode(this.table[col][row].amount);
            td.appendChild(text);
            tr.appendChild(td);
        }
        var tdSum = document.createElement('td');

        var textSum = document.createTextNode(this.getRowSum(row));
        tdSum.className = 'textSum';
        tdSum.appendChild(textSum);
        tdSum.setAttribute('data-row', row);
        tr.appendChild(tdSum);
        var textRemove = document.createTextNode('x');
        var tdRemove = document.createElement('td');
        tdRemove.appendChild(textRemove);
        tdRemove.setAttribute('data-row', row);
        tdRemove.className = 'textRemove';
        tr.appendChild(tdRemove);

        return tr;
    },
    removeRow: function (target) {
        for (var col = 0; col < this.table.length; col++) {
            this.table[col].splice(target.dataset.row, 1);
        }


        document.getElementsByTagName('thead')[0].remove();
        document.getElementsByTagName('tbody')[0].remove();
        this.render();


    },
    addCol: function () {
        for (var col = 0; col < this.table.length; col++) {
            var item = {
                id: this.id++,
                amount: Math.floor(Math.random() * 1000)
            };
            this.table[col].push(item);
        }
        var table = document.getElementById('table');
        var tbody = table.getElementsByTagName('tbody')[0];

        table.getElementsByTagName('thead')[0].remove();
        tbody.appendChild(this.renderMatrix(this.table[0].length - 1));
        table.appendChild(this.renderPercent());

    },
    incrementItem: function (e) {
        this.table[e.dataset.col][e.dataset.row].amount++;
        var tr = e.parentNode;
        var tbody = tr.parentNode;
        tbody.insertBefore(this.renderMatrix(e.dataset.row), tr);
        tr.remove();

        var table = document.getElementById('table');
        table.getElementsByTagName('thead')[0].remove();
        table.appendChild(this.renderPercent());
    },
    hideX: function(target){
        var arr = document.getElementsByClassName('showX');
        target.className = 'textVal';
        for (var i = 0; i<arr.length; i++){
            arr[i].className = 'textVal';
        }
    },
    showX: function (target) {
        var amount = this.table[target.dataset.col][target.dataset.row].amount;
        var result = [];
        var huevoe;
        var item;
        for (var col = 0; col < this.table.length; col++) {
            for (var row = 0; row < this.table[col].length; row++) {
                if (result.length < this.x) {
                    this.table[col][row].buff = Math.abs(this.table[col][row].amount - amount);
                    result.push(this.table[col][row]);
                } else {
                    this.table[col][row].buff = Math.abs(this.table[col][row].amount - amount);
                    for (var i = 0; i < result.length; i++) {
                        if (!huevoe || result[i].buff > huevoe) {
                            huevoe = result[i].buff;
                            item = i;
                        }
                    }
                    if(huevoe > this.table[col][row].buff){
                        result.splice(item, 1);
                        result.push(this.table[col][row]);
                        console.log(result[0].amount);
                        huevoe = this.table[col][row].buff;
                        item = result.length - 1;
                    }
                }
            }
        }
        for (var item = 0; item < result.length; item++) {
            document.getElementById(result[item].id).className = 'showX textVal';
        }
    }

};

var matrix = new Matrix(5, 5, 2);
if (matrix.table.length > 0) {

    matrix.render();

    document.getElementById('addCol').addEventListener('click', function () {
        matrix.addCol();
    });

    document.querySelector('body').addEventListener('click', function (event) {
        var target = event.target;

        if (target.className == 'showX textVal') {
            matrix.incrementItem(target);
        }

        if (target.className == 'textRemove') {
            if (matrix.table[0].length > 1) {
                matrix.removeRow(target);
            } else {
                alert('cant delete last row');
            }
        }

    });

    document.querySelector('body').addEventListener('mouseover', function (event) {
        var target = event.target;

        if (target.className == 'textVal') {
            matrix.showX(target);
        }

    })

    document.querySelector('body').addEventListener('mouseout', function(event){
        var target = event.target;

        if (target.className == 'showX textVal'){
            matrix.hideX(target);
        }

    })
}
